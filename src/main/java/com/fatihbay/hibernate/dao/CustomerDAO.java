package com.fatihbay.hibernate.dao;

import com.fatihbay.hibernate.model.Customer;

public interface CustomerDAO extends GenericDAO<Customer, Long> {

}

package com.fatihbay.hibernate.dao;

import com.fatihbay.hibernate.model.Item;

public interface ItemDAO extends GenericDAO<Item, Long> {
	
}

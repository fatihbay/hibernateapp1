package com.fatihbay.hibernate.dao;

import org.hibernate.SessionFactory;

import com.fatihbay.hibernate.model.Item;

public class ItemDAOHibernate 
         extends GenericHibernateDAO<Item, Long>  
         implements ItemDAO {

	public ItemDAOHibernate() {
		super();
	}

	public ItemDAOHibernate(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
}

package com.fatihbay.hibernate.dao;

import com.fatihbay.hibernate.model.Category;

public interface CategoryDAO extends GenericDAO<Category, Long> {

}

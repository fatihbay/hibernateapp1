package com.fatihbay.hibernate.dao;

import org.hibernate.SessionFactory;

import com.fatihbay.hibernate.model.Customer;

public class CustomerDAOHibernate 
          extends GenericHibernateDAO<Customer, Long>
          implements CustomerDAO {

	public CustomerDAOHibernate() {
		super();
	}

	public CustomerDAOHibernate(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
}

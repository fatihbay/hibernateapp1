package com.fatihbay.hibernate.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public abstract class GenericHibernateDAO<T, ID extends Serializable>
        implements GenericDAO<T, ID> {

	// @Autowired
	private SessionFactory sessionFactory;
	private Class<T> persistentClass;

	public GenericHibernateDAO() { 
		super();
	}

	public GenericHibernateDAO(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
	}

	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setPersistentClass(Class<T> persistentClass) {
		this.persistentClass = persistentClass;
	}

	@Override
	public void delete(T entity) {
		getCurrentSession().delete(entity);
	}

	@Override
	public void deleteById(ID id) {
		T entity = findById(id);
		delete(entity);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		return getCurrentSession().createQuery("from " + persistentClass.getName()).list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public T findById(ID id) {
		return (T) getCurrentSession().get(persistentClass, id);
	}

	@Override
	public void save(T entity) {
		getCurrentSession().persist(entity);
	}

	@Override
	public void update(T entity) {
		getCurrentSession().merge(entity);
	}
}
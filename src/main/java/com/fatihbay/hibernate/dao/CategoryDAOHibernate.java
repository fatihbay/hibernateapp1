package com.fatihbay.hibernate.dao;

import org.hibernate.SessionFactory;

import com.fatihbay.hibernate.model.Category;

public class CategoryDAOHibernate 
          extends GenericHibernateDAO<Category, Long> 
          implements CategoryDAO {

	public CategoryDAOHibernate() {
		super();
	}

	public CategoryDAOHibernate(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
}

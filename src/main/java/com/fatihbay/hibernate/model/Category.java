package com.fatihbay.hibernate.model;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Category {
	
	@Id
	@GeneratedValue
	private Long id;
	private String name;

    @OneToMany(mappedBy="category") // other side is owner
	private Collection<Item> items = new HashSet<Item>();
    
    @ManyToOne
    @JoinColumn(name="parentCategory_id")
    private Category parentCategory;

    @OneToMany (mappedBy="parentCategory")
    private Collection<Category> subCategories = new HashSet<Category>();

    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Collection<Item> getItems() {
		return items;
	}
	public void setItems(Collection<Item> items) {
		this.items = items;
	}
	
	public void addItem(Item item) {
		if (null != item.getCategory()) {
			Category otherCategory = item.getCategory();
			otherCategory.getItems().remove(item);
		}
		items.add(item);
		item.setCategory(this);
	}
	
	public Category getParentCategory() {
		return parentCategory;
	}
	
	public void setParentCategory(Category parentCategory) {
		this.parentCategory = parentCategory;
	}
	
	public void addSubCategory(Category subCategory) {
		if (null != subCategory.getParentCategory()) {
			Category otherParentCategory = subCategory.getParentCategory();
			otherParentCategory.getSubCategories().remove(subCategory);
		}
		
		subCategories.add(subCategory);
		subCategory.setParentCategory(this);
	}
	
	public Collection<Category> getSubCategories() {
		return subCategories;
	}
	public void setSubCategories(Collection<Category> subCategories) {
		this.subCategories = subCategories;
	}
	@Override
	public String toString() {
		return "Category [id=" + id + ", name=" + name + "]";
	}
}

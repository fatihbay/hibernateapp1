package com.fatihbay.hibernate.model;

import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Customer {

	@Id
	@GeneratedValue
	private Long id;
	private String username;

	@ManyToMany // owner side of relationship
	private Collection<Item> items = new HashSet<Item>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Collection<Item> getItems() {
		return items;
	}

	public void setItems(Collection<Item> items) {
		this.items = items;
	}
	
	public void addItem(Item item) {
		items.add(item);
		item.getCustomers().add(this);
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", username=" + username + "]";
	}
}

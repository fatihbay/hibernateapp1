package com.fatihbay.hibernate;

import com.fatihbay.hibernate.dao.CategoryDAO;
import com.fatihbay.hibernate.dao.CategoryDAOHibernate;
import com.fatihbay.hibernate.dao.CustomerDAO;
import com.fatihbay.hibernate.dao.CustomerDAOHibernate;
import com.fatihbay.hibernate.dao.ItemDAO;
import com.fatihbay.hibernate.dao.ItemDAOHibernate;
import com.fatihbay.hibernate.model.Category;
import com.fatihbay.hibernate.model.Customer;
import com.fatihbay.hibernate.model.Item;
import com.fatihbay.hibernate.util.HibernateUtil;

/**
 * Hello Hibernate!
 * 
 */
public class App {
	public static void main(String[] args) {

		HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
		
		// create DAOs
		ItemDAO itemDAO = new ItemDAOHibernate(HibernateUtil.getSessionFactory());
		CustomerDAO customerDAO = new CustomerDAOHibernate(HibernateUtil.getSessionFactory());
		CategoryDAO categoryDAO = new CategoryDAOHibernate(HibernateUtil.getSessionFactory());

		// create Items
		Item item1 = new Item();
		item1.setName("Asus P67");
		Item item2 = new Item();
		item2.setName("Intel i3 3110m");
		Item item3 = new Item();
		item3.setName("Microsoft Windows XP");
		Item item4 = new Item();
		item4.setName("Microsoft Windows 7");
		Item item5 = new Item();
		item5.setName("Age of Empires");
		Item item6 = new Item();
		item6.setName("Halflife");
		Item item7 = new Item();
		item7.setName("Counter Strike");

		// create Customers
		Customer customer1 = new Customer();
		customer1.setUsername("Customer 1");
		Customer customer2 = new Customer();
		customer2.setUsername("Customer 2");
		Customer customer3 = new Customer();
		customer3.setUsername("Customer 3");
		
		// create Categories
		Category category0 = new Category(); 
		category0.setName("Root");
		
		Category category1 = new Category(); 
		category1.setName("Hardware");
		category1.setParentCategory(category0);
		
		Category category2 = new Category(); 
		category2.setName("Software");
		category2.setParentCategory(category0);
		
		Category category3 = new Category(); 
		category3.setName("Mainboard");
		category3.setParentCategory(category1);
		
		Category category4 = new Category(); 
		category4.setName("Processor");
		category4.setParentCategory(category1);
		
		Category category5 = new Category(); 
		category5.setName("Games");
		category5.setParentCategory(category2);
		
		Category category6 = new Category(); 
		category6.setName("Strategy");
		category6.setParentCategory(category5);
		
		Category category7 = new Category(); 
		category7.setName("First Person");
		category7.setParentCategory(category5);
		
		Category category8 = new Category(); 
		category8.setName("Operating System");
		category8.setParentCategory(category2);
		
		// mapping customer and item
		item1.addCustomer(customer1);
		item2.addCustomer(customer1);
		item2.addCustomer(customer3);
		item3.addCustomer(customer2);
		item4.addCustomer(customer2);
		item5.addCustomer(customer3);
		item6.addCustomer(customer3);
		item7.addCustomer(customer1);
		
		// mapping item and category
		item1.setCategory(category3);
		item2.setCategory(category4);
		item3.setCategory(category8);
		item4.setCategory(category8);
		item5.setCategory(category6);
		item6.setCategory(category7);
		item7.setCategory(category7);

		// save all objects separately, !!! NO CASCADING DEFINED
		itemDAO.save(item1);
		itemDAO.save(item1);
		itemDAO.save(item2);
		itemDAO.save(item3);
		itemDAO.save(item4);
		itemDAO.save(item5);
		itemDAO.save(item6);
		itemDAO.save(item7);

		categoryDAO.save(category0);
		categoryDAO.save(category1);
		categoryDAO.save(category2);
		categoryDAO.save(category3);
		categoryDAO.save(category4);
		categoryDAO.save(category5);
		categoryDAO.save(category6);
		categoryDAO.save(category7);
		categoryDAO.save(category8);
		
		customerDAO.save(customer1);
		customerDAO.save(customer2);
		customerDAO.save(customer3);

		HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
	}
}
